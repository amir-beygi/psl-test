# PSL Group Test

By amir beygi

* Install the application
    * Clone the repo
    - `git clone git@bitbucket.org:amir-beygi/psl-test.git`
    * Execute `./composer.phar install`
* Configure your database connection configuration, or just the one I sent you
    - `phonebook/app/config/parameters.yml`
* Install database tables, if you are using your own database
    - `php bin/console doctrine:schema:update --force`    
* Run your application:
    1. Execute `php bin/console server:start`
    2. Browse to the http://localhost:8000 URL.

